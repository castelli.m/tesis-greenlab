# RPi
import time 
import datetime
import RPi.GPIO as GPIO 
import paho.mqtt.client as mqtt
import serial
import json
import requests

# Variables
tin=0
tfin=5
entro=0
entrol=0
tinl=7
tfinl=23
temperaturaDeseada=29.5
umbralNitro=200
umbralPotasio=200
umbralFosforo=30
diccionario=dict()

# Conexiones Seriales
arduino = serial.Serial("/dev/ttyACM0", baudrate=9600)
arduino2= serial.Serial("/dev/ttyACM1", baudrate=9600)

# Configuration: 
LED_PIN        = 24 
BUTTON_PIN     = 23 
# Initialize GPIO for LED and button. 
GPIO.setmode(GPIO.BCM) 
GPIO.setwarnings(False) 
GPIO.setup(LED_PIN, GPIO.OUT) 
GPIO.output(LED_PIN, GPIO.LOW) 
GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) 
# Setup callback functions that are called when MQTT events happen like 
# connecting to the server or receiving data from a subscribed feed. 
def on_connect(client, userdata, flags, rc): 
   print("Connected with result code " + str(rc)) 
  
# The callback for when a PUBLISH message is received from the server. 
def on_message(client, userdata, msg): 
   print(msg.topic+" "+str( msg.payload)) 
   
# Create MQTT client and connect to localhost, i.e. the Raspberry Pi running 
# this script and the MQTT server. 
client = mqtt.Client() 
client.on_connect = on_connect 
client.on_message = on_message 
client.connect('localhost', 1883, 60) 

# Connect to the MQTT server and process messages in a background thread. 
client.loop_start() 

# Main loop to listen for button presses. 
print('Script is running, press Ctrl-C to quit...') 
time.sleep(1)

def heatON():
      #Ventiladores
      client.publish("b4:e6:2d:03:ac:01", 1)
      client.publish("b4:e6:2d:03:ac:01", 3)
      #Calo
      client.publish("b4:e6:2d:03:c2:42", 1)
      client.publish("b4:e6:2d:03:45:d4", 3)
def coldON():
      #Frio
      client.publish("b4:e6:2d:03:c2:42", 3)
      client.publish("b4:e6:2d:03:45:d4", 1)

def heatOFF():
      #Ventiladores
      client.publish("b4:e6:2d:03:ac:01", 0)
      client.publish("b4:e6:2d:03:ac:01", 2)
      #Calo
      client.publish("b4:e6:2d:03:c2:42", 0)
      client.publish("b4:e6:2d:03:45:d4", 2)

def coldOFF():
      #Frio
      client.publish("b4:e6:2d:03:c2:42", 2)
      client.publish("b4:e6:2d:03:45:d4", 0)

def fanON():
      #Frio
      client.publish("b4:e6:2d:03:ac:01", 3)
      client.publish("b4:e6:2d:03:ac:01", 1)      

def fanOFF():
      #Frio
      client.publish("b4:e6:2d:03:ac:01", 2)
      client.publish("b4:e6:2d:03:ac:01", 0)      

def perception():
   txt="" 
   txt2=""
   caracter=""	
   inicio=False
   fin=False
   arduino.write("S")
   
   while arduino.inWaiting() > 0 and not inicio:
	caracter=arduino.read(1)
	if(caracter=="{"):
		inicio=True
   txt=caracter
   while arduino.inWaiting() > 0 and not fin:
	     caracter=arduino.read(1)
	     if(caracter!="}"):
	     	txt += caracter
		time.sleep(0.0005)
	     else:
		fin=True
		txt += caracter
   inicio=False
   fin=False

   arduino2.write("D")	
   while arduino2.inWaiting() > 0 and not inicio:

        caracter=arduino2.read(1)
        if(caracter=="{"):
                inicio=True
   txt2=caracter
   while arduino2.inWaiting() > 0 and not fin:

             caracter=arduino2.read(1)
             if(caracter!="}"):
                txt2 += caracter;
		time.sleep(0.0005);
             else:
                fin=True
                txt2 += caracter

   
   txt_json=json.dumps(txt)
   stringMFJ=json.loads(txt_json)
   stringMFJ=txt[1:len(txt)-1]
   #print stringMFJ
   arr=stringMFJ.split(",")
   #print arr
   if arr:
   	for x in arr:
		arr2=x.split(":")
		#print len(arr2)
		if len(arr2)>1:
			diccionario[arr2[0][1:len(arr2[0])-1]]= arr2[1]  
   txt2_json=json.dumps(txt2)
   string2MFJ=json.loads(txt2_json)
   string2MFJ=txt2[1:len(txt2)-1]
   arr=string2MFJ.split(",")
   if arr:
        for x in arr:
                arr2=x.split(":")
                if len(arr2)>1:
                        diccionario[arr2[0][1:len(arr2[0])-1]]= arr2[1]  

def step():
   ahora = datetime.datetime.now()
   if(ahora.hour>=tinl and ahora.hour<=tfinl):
        client.publish("b4:e6:2d:03:43:c3",1)
	client.publish("b4:e6:2d:03:43:c3",3)
	client.publish("b4:e6:2d:03:46:44",3)
	
	if(ahora.minute>=tin and ahora.minute<=tfin):
                client.publish("b4:e6:2d:03:ac:01",1)
                client.publish("b4:e6:2d:03:ac:01",3)
                client.publish("b4:e6:2d:04:1f:31",1)
		
                entro=1
        elif(ahora.minute>=tin and ahora.minute<=tfin):
                
                toma=1
        else:
                client.publish("b4:e6:2d:03:ac:01",0)
                client.publish("b4:e6:2d:03:ac:01",2)
                client.publish("b4:e6:2d:04:1f:31",0)
                entro=0
       
	time.sleep(1)        
	entrol=1
   elif(ahora.hour>=tinl and ahora.hour<=tfinl):
	toma=1
   else:
        client.publish("b4:e6:2d:03:43:c3",0)
	client.publish("b4:e6:2d:03:43:c3",2)
	client.publish("b4:e6:2d:03:46:44",2)
        entrol=0

   time.sleep(1)
   #toma la percepcion del entorno
   perception()

   print "Diccionario"
   if diccionario:
		print diccionario
		print diccionario['temperatura']
		
		
   if(diccionario):
	   if(diccionario['temperatura']):
		if(float(diccionario['temperatura'])<temperaturaDeseada):
			coldOFF() 
			#Se encienden los dos caloventiladores 
			heatON()
			fanON()
		else:
			heatOFF()
			fanOFF()
			coldON()

while True: 
   step()
		  
	  
arduino.close() 
arduino2.close()

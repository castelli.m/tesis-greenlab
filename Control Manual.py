# RPi
import time 
import datetime
import RPi.GPIO as GPIO 
import paho.mqtt.client as mqtt
import serial

entro=0
arduino = serial.Serial("/dev/ttyACM0", baudrate=9600)
# Configuration: 
LED_PIN        = 24 
BUTTON_PIN     = 23 
# Initialize GPIO for LED and button. 
GPIO.setmode(GPIO.BCM) 
GPIO.setwarnings(False) 
GPIO.setup(LED_PIN, GPIO.OUT) 
GPIO.output(LED_PIN, GPIO.LOW) 
GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) 
# Setup callback functions that are called when MQTT events happen like 
# connecting to the server or receiving data from a subscribed feed. 
def on_connect(client, userdata, flags, rc): 
   print("Connected with result code " + str(rc)) 
   # Subscribing in on_connect() means that if we lose the connection and 
   # reconnect then subscriptions will be renewed. 
   client.subscribe("/leds/pi") 
# The callback for when a PUBLISH message is received from the server. 
def on_message(client, userdata, msg): 
   print(msg.topic+" "+str( msg.payload)) 
   # Check if this is a message for the Pi LED. 
   if msg.topic == '/leds/pi': 
       # Look at the message data and perform the appropriate action. 
       if msg.payload == b'ON': 
           GPIO.output(LED_PIN, GPIO.HIGH) 
       elif msg.payload == b'OFF': 
           GPIO.output(LED_PIN, GPIO.LOW) 
       elif msg.payload == b'TOGGLE': 
           GPIO.output(LED_PIN, not GPIO.input(LED_PIN)) 
# Create MQTT client and connect to localhost, i.e. the Raspberry Pi running 
# this script and the MQTT server. 
client = mqtt.Client() 
client.on_connect = on_connect 
client.on_message = on_message 
client.connect('localhost', 1883, 60) 
# Connect to the MQTT server and process messages in a background thread. 
client.loop_start() 
# Main loop to listen for button presses. 
print('Script is running, press Ctrl-C to quit...') 
time.sleep(1)
while True: 
   # Look for a change from high to low value on the button input to 
   # signal a button press. 
   #button_first = GPIO.input(BUTTON_PIN) 
   #time.sleep(0.02)  # Delay for about 20 milliseconds to debounce. 
   #button_second = GPIO.input(BUTTON_PIN) 
   #if button_first == GPIO.HIGH and button_second == GPIO.LOW: 
   #    print('Button pressed!') 
       # Send a toggle message to the ESP8266 LED topic. 
   ahora = datetime.datetime.now()
#   print(ahora.hour)
#   print(ahora.minute)
	
   print('---------------OPCIONES----------------')
   print('1-Motor')
   print('2-Bomba + Luz Cuadrada')
   print('3-Luces de amplio espectro')
   print('4-Vaporizador + Camara')
   print('5-Ventilador D + Ventilador I') 
   print('6-Calo I + Refri I') 
   print('7-Refri D + Calo D')  
   print('8-Peristalticas') 
   print('----------------------------------------')
   opcion= input('Ingrese una opcion: ') 
   topico="prueba" 
   
   if opcion==1:
      topico="b4:e6:2d:00:ee:bc"
   if opcion==2:
      topico="b4:e6:2d:03:46:44"
   if opcion==3:
      topico="b4:e6:2d:03:43:c3"
   if opcion==4:
      topico="b4:e6:2d:04:1f:31"
   if opcion==5:
      topico="b4:e6:2d:03:ac:01"
   if opcion==6:
      topico="b4:e6:2d:03:c2:42"
   if opcion==7:
      topico="b4:e6:2d:03:45:d4"
   if opcion==8:
      time.sleep(0.1) #wait for serial to open
      if arduino.isOpen():
         operacionSerial=raw_input('Ingrese la operacion: ')
         arduino.write(operacionSerial.encode())
   else:
      operacion=input('Ingrese la operacion: ')
      client.publish(topico, operacion)
      


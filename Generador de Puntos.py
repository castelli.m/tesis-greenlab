# RPi
import time 
import datetime
import RPi.GPIO as GPIO 
import paho.mqtt.client as mqtt
import serial
import json
import requests

tin=0
tfin=5
entro=0
entrol=0
tinl=7
tfinl=23
umbralNitro=200
umbralPotasio=200
umbralFosforo=30
arduino = serial.Serial("/dev/ttyACM0", baudrate=9600)
arduino2= serial.Serial("/dev/ttyACM1", baudrate=9600)
# Configuration: 
LED_PIN        = 24 
BUTTON_PIN     = 23 
# Initialize GPIO for LED and button. 
GPIO.setmode(GPIO.BCM) 
GPIO.setwarnings(False) 
GPIO.setup(LED_PIN, GPIO.OUT) 
GPIO.output(LED_PIN, GPIO.LOW) 
GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP) 
# Setup callback functions that are called when MQTT events happen like 
# connecting to the server or receiving data from a subscribed feed. 
def heatON():
      #Ventiladores
      client.publish("b4:e6:2d:03:ac:01", 1)
      client.publish("b4:e6:2d:03:ac:01", 3)
      #Calo
      client.publish("b4:e6:2d:03:c2:42", 1)
      client.publish("b4:e6:2d:03:45:d4", 3)
def coldON():
      #Frio
      client.publish("b4:e6:2d:03:c2:42", 3)
      client.publish("b4:e6:2d:03:45:d4", 1)

def heatOFF():
      #Ventiladores
      client.publish("b4:e6:2d:03:ac:01", 0)
      client.publish("b4:e6:2d:03:ac:01", 2)
      #Calo
      client.publish("b4:e6:2d:03:c2:42", 0)
      client.publish("b4:e6:2d:03:45:d4", 2)

def coldOFF():
      #Frio
      client.publish("b4:e6:2d:03:c2:42", 2)
      client.publish("b4:e6:2d:03:45:d4", 0)

def fanON():
      #Frio
      client.publish("b4:e6:2d:03:ac:01", 3)
      client.publish("b4:e6:2d:03:ac:01", 1)      

def fanOFF():
      #Frio
      client.publish("b4:e6:2d:03:ac:01", 2)
      client.publish("b4:e6:2d:03:ac:01", 0)      
 

def on_connect(client, userdata, flags, rc): 
   print("Connected with result code " + str(rc)) 
   # Subscribing in on_connect() means that if we lose the connection and 
   # reconnect then subscriptions will be renewed. 
   client.subscribe("/leds/pi") 
# The callback for when a PUBLISH message is received from the server. 
def on_message(client, userdata, msg): 
   print(msg.topic+" "+str( msg.payload)) 
   # Check if this is a message for the Pi LED. 
   if msg.topic == '/leds/pi': 
       # Look at the message data and perform the appropriate action. 
       if msg.payload == b'ON': 
           GPIO.output(LED_PIN, GPIO.HIGH) 
       elif msg.payload == b'OFF': 
           GPIO.output(LED_PIN, GPIO.LOW) 
       elif msg.payload == b'TOGGLE': 
           GPIO.output(LED_PIN, not GPIO.input(LED_PIN)) 
# Create MQTT client and connect to localhost, i.e. the Raspberry Pi running 
# this script and the MQTT server. 
client = mqtt.Client() 
client.on_connect = on_connect 
client.on_message = on_message 
client.connect('localhost', 1883, 60) 
# Connect to the MQTT server and process messages in a background thread. 
client.loop_start() 
# Main loop to listen for button presses. 
print('Script is running, press Ctrl-C to quit...') 
time.sleep(1)


accionAnt=-1
while True: 
   # Look for a change from high to low value on the button input to 
   # signal a button press. 
   #button_first = GPIO.input(BUTTON_PIN) 
   #time.sleep(0.02)  # Delay for about 20 milliseconds to debounce. 
   #button_second = GPIO.input(BUTTON_PIN) 
   #if button_first == GPIO.HIGH and button_second == GPIO.LOW: 
   #    print('Button pressed!') 
       # Send a toggle message to the ESP8266 LED topic. 
   ahora = datetime.datetime.now()
   #print(ahora.hour)
   #print(ahora.minute)
   #print(entro)
   accion= input('Ingrese una accion: ') 
   
   if accion==0:
	   if accionAnt==accion:
		   nada=1
	   else:
		   heatOFF()
		   coldON()
   if accion==1:
	   if accionAnt==accion:
		   nada=1
	   else:
		coldOFF()
		heatON()
   if accion==2:
	   if accionAnt==accion:
		   nada=1
	   else:
		coldOFF()
		heatOFF()
		fanOFF()
   if accion==3:
	   if accionAnt==accion:
		   nada=1
	   else:
		coldOFF()
		heatOFF()
		fanON()
   accionAnt=accion

   

   txt="" 
   txt2=""
   caracter=""	
   inicio=False
   fin=False
   arduino.write("S")
   time.sleep(0.5)   
   while arduino.inWaiting() > 0 and not inicio:
	caracter=arduino.read(1)
	if(caracter=="{"):
		inicio=True
   txt=caracter
   while arduino.inWaiting() > 0 and not fin:
	     caracter=arduino.read(1)
	     if(caracter!="}"):
	     	txt += caracter
		time.sleep(0.0005)
	     else:
		fin=True
		txt += caracter
   
   arduino2.write("D")
   time.sleep(0.5)
   inicio=False
   fin=False
   while arduino2.inWaiting() > 0 and not inicio:
        caracter=arduino2.read(1)
        if(caracter=="{"):
                inicio=True
   txt2=caracter
   while arduino2.inWaiting() > 0 and not fin:
             caracter=arduino2.read(1)
             if(caracter!="}"):
                txt2 += caracter;
		time.sleep(0.0005);
             else:
                fin=True
                txt2 += caracter
           
   diccionario=dict()
   
   
   txt_json=json.dumps(txt)
   stringMFJ=json.loads(txt_json)
   stringMFJ=txt[1:len(txt)-1]
   arr=stringMFJ.split(",")
   if arr:
   	for x in arr:
		arr2=x.split(":")
		if len(arr2)>1:
			diccionario[arr2[0][1:len(arr2[0])-1]]= arr2[1]  
   txt2_json=json.dumps(txt2)
   string2MFJ=json.loads(txt2_json)
   string2MFJ=txt2[1:len(txt2)-1]
   arr=string2MFJ.split(",")
   if arr:
        for x in arr:
                arr2=x.split(":")
                if len(arr2)>1:
                        diccionario[arr2[0][1:len(arr2[0])-1]]= arr2[1]  

   print "Diccionario"
   if diccionario:
		print diccionario
		print diccionario['temperatura']

   
#   	client.publish(topico, operacion)
arduino.close() 
arduino2.close()

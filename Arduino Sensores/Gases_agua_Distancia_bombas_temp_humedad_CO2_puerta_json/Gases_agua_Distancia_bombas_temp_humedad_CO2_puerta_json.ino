//temperarura y humedad
#include "Adafruit_HTU21DF.h"
#include <Wire.h>
#include <MHZ.h>
//Luz
#include <BH1750.h>
// Gases
#include "MutichannelGasSensor.h"
#include <ArduinoJson.h>

#define CO2_IN 6
#define PUERTA A1
// pin for uart reading
#define MH_Z19_RX 4  // D7
#define MH_Z19_TX 5  // D6

long tiempo_ant = 0;
long tiempo_act = 0;
long tiempo_mensaje=0;

//Distancia
const int EchoPin = 3;
const int TriggerPin = 12;

//Variables
float temp;
float rel_hum;
float nh3;
float co;
float no2;
float c3h8;
float c4h10;
float ch4;
float h2;
float c2h5oh;
uint16_t lux;
int cm;
int ppm_pwm;
int puerta;
int agua;
bool midio=false;

BH1750 lightMeter;

Adafruit_HTU21DF htu = Adafruit_HTU21DF();

SoftwareSerial mySerial(MH_Z19_TX, MH_Z19_RX);
MHZ co2(&mySerial, CO2_IN, MHZ19B);

StaticJsonDocument<500> doc;

String ser;
void readSerialPort() {
  ser = "";
  if (Serial.available()) {
    delay(10);
    while (Serial.available() > 0) {
      ser += (char)Serial.read();
    }
    Serial.flush();
  }
}

void setup() {
  pinMode(TriggerPin, OUTPUT);
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(EchoPin, INPUT);
  digitalWrite(7, HIGH);
  digitalWrite(8, HIGH);
  digitalWrite(9, HIGH);
  digitalWrite(10, HIGH);
  digitalWrite(11, HIGH);
  Serial.begin(9600);


  //Temperatura + humedad DHT21
  htu.begin();

  //Gases aire
  gas.begin(0x04);//the default I2C address of the slave is 0x04
  gas.powerOn();
  //Serial.print("Firmware Version = ");
  //Serial.println(gas.getVersion());
  // Luz
  Wire.begin();
  lightMeter.begin();

  //CO2
  mySerial.begin(9600);
  pinMode(CO2_IN, INPUT);

}

int ping(int TriggerPin, int EchoPin) {
  long duration, distanceCm;

  digitalWrite(TriggerPin, LOW);  //para generar un pulso limpio ponemos a LOW 4us
  delayMicroseconds(4);
  digitalWrite(TriggerPin, HIGH);  //generamos Trigger (disparo) de 10us
  delayMicroseconds(10);
  digitalWrite(TriggerPin, LOW);

  duration = pulseIn(EchoPin, HIGH);  //medimos el tiempo entre pulsos, en microsegundos

  distanceCm = duration * 10 / 292 / 2;  //convertimos a distancia, en cm
  return distanceCm;
}


void loop() {
  tiempo_act = millis();
  if (tiempo_act - tiempo_ant < 10000 && !midio) {
    midio=true;
    //agua=analogRead(A0);
    //Serial.println("Datos");
    //Medición de agua
    //Serial.print("Agua: ");
    //Serial.println(agua);

    //Temperatura + humedad DHT21
    temp = htu.readTemperature();
    rel_hum = htu.readHumidity();
//    Serial.print("Temp: "); Serial.print(temp); Serial.println(" C");
//    Serial.print("Humidity: "); Serial.print(rel_hum); Serial.println(" \%");

    //Gases

    float c;

    nh3 = gas.measure_NH3();
//    Serial.print("NH3:");
//    if (nh3 >= 0) {
//      Serial.print(nh3);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    co = gas.measure_CO();
//    Serial.print("CO: ");
//    if (co >= 0) {
//      Serial.print(co);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    no2 = gas.measure_NO2();
//    Serial.print("NO2: ");
//    if (no2 >= 0) {
//      Serial.print(no2);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    c3h8 = gas.measure_C3H8();
//    Serial.print("C3H8: ");
//    if (c3h8 >= 0) {
//      Serial.print(c3h8);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    c4h10 = gas.measure_C4H10();
//    Serial.print("C4H10: ");
//    if (c4h10 >= 0) {
//      Serial.print(c4h10);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    ch4 = gas.measure_CH4();
//    Serial.print("CH4: ");
//    if (ch4 >= 0) {
//      Serial.print(ch4);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    h2 = gas.measure_H2();
//    Serial.print("H2: ");
//    if (h2 >= 0) {
//      Serial.print(h2);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    c2h5oh = gas.measure_C2H5OH();
//    Serial.print("C2H5OH: ");
//    if (c2h5oh >= 0) {
//      Serial.print(c2h5oh);
//    } else {
//      Serial.print("invalid");
//    }
//    Serial.println(" ppm");

    //Luz
    lux = lightMeter.readLightLevel();//Realizamos una lectura del sensor
    //Serial.print("Luz(iluminancia):  ");
    //Serial.print(lux);
    //Serial.println(" lx");

    //Distancia

    cm = ping(TriggerPin, EchoPin);
    //Serial.print("Distancia: ");
    //Serial.println(cm);

    //CO2
    ppm_pwm = co2.readCO2PWM();
    //Serial.print("CO2 PPM: ");
    //Serial.println(ppm_pwm);

    //PUERTA

    int valorPuerta=analogRead(A1);
    if(valorPuerta>500){
      puerta=1;
      //Serial.println("Puerta: Abierta");
    }else{
      puerta=0;
      //Serial.println("Puerta: Cerrada");
    }

  }
  if (tiempo_act - tiempo_ant >= 10000) {
    midio=false;
    tiempo_ant=tiempo_act;
  }

  if(tiempo_act - tiempo_mensaje >= 2000) {
    tiempo_mensaje=tiempo_act;
  
    doc["temperatura"]=temp;
    doc["humedad"]=rel_hum;
    doc["amoniaco"]=nh3;
    doc["co"]=co;
    doc["no2"]=no2;
    doc["propano"]=c3h8;
    doc["butano"]=c4h10;
    doc["metano"]=ch4;
    doc["hidrogeno"]=h2;
    doc["etanol"]=c2h5oh;
    doc["luminosidad"]=lux;
    doc["distancia"]=cm;
    doc["co2"]=ppm_pwm;
    doc["puerta"]=puerta;
    //serializeJsonPretty(doc, Serial);
  }
  readSerialPort();
  
  if (ser == "P-N-0") {
    digitalWrite(7, HIGH);
  } else if (ser == "P-N-1") {
    digitalWrite(7, LOW);
  } else if (ser == "P-P-0") {
    digitalWrite(8, HIGH);
  } else if (ser == "P-P-1") {
    digitalWrite(8, LOW);
  } else if (ser == "P-K-0") {
    digitalWrite(9, HIGH);
  } else if (ser == "P-K-1") {
    digitalWrite(9, LOW);
  } else if (ser == "P-A-0") {
    digitalWrite(10, HIGH);
  } else if (ser == "P-A-1") {
    digitalWrite(10, LOW);
  } else if (ser == "P-B-0") {
    digitalWrite(11, HIGH);
  } else if (ser == "P-B-1") {
    digitalWrite(11, LOW);
  } else if (ser == "S") {
    doc["temperatura"]=temp;
    doc["humedad"]=rel_hum;
    doc["amoniaco"]=nh3;
    doc["co"]=co;
    doc["no2"]=no2;
    doc["propano"]=c3h8;
    doc["butano"]=c4h10;
    doc["metano"]=ch4;
    doc["hidrogeno"]=h2;
    doc["etanol"]=c2h5oh;
    doc["luminosidad"]=lux;
    doc["distancia"]=cm;
    doc["co2"]=ppm_pwm;
    doc["puerta"]=puerta;
    serializeJson(doc, Serial);
    //serializeJson(doc, Serial);
  }else if (ser.substring(0,1) == "D") {
    //Serial.println(ser.substring(2,ser.length()));
    int distanciaDeseada=ser.substring(2,ser.length()).toInt();
    Serial.println(distanciaDeseada);
    
  }

  
 
  // put your main code here, to run repeatedly:

}

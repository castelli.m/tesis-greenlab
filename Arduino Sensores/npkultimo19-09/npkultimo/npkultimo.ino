
#include <SoftwareSerial.h>
#include <Wire.h>
#include <DHT.h>
// Include Emon Library
#include "EmonLib.h"
#include <ArduinoJson.h>
#include "DFRobot_PH.h"
#include <EEPROM.h>

StaticJsonDocument<200> doc;

#define DHTPIN 6     
#define DHTTYPE DHT22   
DHT dht(DHTPIN, DHTTYPE); 

float hum;  
float tempe;


#define RE 8
#define DE 7

#define SensorPin A1
#define WaterPin A2 
// the pH meter Analog output is connected with the Arduino’s Analog
unsigned long int avgValue;  //Store the average value of the sensor feedback
float b;
int buf[10],temp;

//const byte code[]= {0x01, 0x03, 0x00, 0x1e, 0x00, 0x03, 0x65, 0xCD};
const byte nitro[] = {0x01,0x03, 0x00, 0x1e, 0x00, 0x01, 0xe4, 0x0c};
const byte phos[] = {0x01,0x03, 0x00, 0x1f, 0x00, 0x01, 0xb5, 0xcc};
const byte pota[] = {0x01,0x03, 0x00, 0x20, 0x00, 0x01, 0x85, 0xc0};
 
byte values[11];
SoftwareSerial mod(2,3);

// Crear una instancia EnergyMonitor
EnergyMonitor energyMonitor;

// Voltaje de nuestra red eléctrica
float voltajeRed = 232.0;

String ser;
double Irms;
double potencia;
unsigned long tiempo_ant;
unsigned long tiempo_act;
unsigned long tiempo_mensaje;
int water;
byte nitrogeno;
byte phosforo;
byte potasio;
bool sondaTomoMedida=false;
bool phTomoMedida=false;
bool restoTomoMedida=false;

float voltage,phValue,temperature = 25;
DFRobot_PH ph;
void readSerialPort() {
  ser = "";
  if (Serial.available()) {
    delay(10);
    while (Serial.available() > 0) {
      ser += (char)Serial.read();
    }
    Serial.flush();
  }
}

void setup() {
  Serial.begin(9600);
  mod.begin(9600);
  pinMode(RE, OUTPUT);
  pinMode(DE, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(5,OUTPUT);
  pinMode(4,OUTPUT);
  pinMode(9,OUTPUT);
  pinMode(6,INPUT);
  //sensor consumo
  //pinMode(12, OUTPUT);
  pinMode(13, INPUT);
  energyMonitor.current(0, 30);
  
  //digitalWrite(12,HIGH);
  digitalWrite(10,LOW);
  digitalWrite(11,HIGH);
  digitalWrite(5,HIGH);
  //reles apagar ph
  digitalWrite(9,HIGH);
  digitalWrite(10,HIGH);

  dht.begin();
  delay(1000);
}

void medirPH(){
  
  //delay(2000);
  voltage = analogRead(SensorPin)/1024.0*5000;  // read the voltage
  phValue = ph.readPH(voltage,temperature);
  
  //digitalWrite(9,LOW);
}

int ping(int TriggerPin, int EchoPin) {
  long duration, distanceCm;

  digitalWrite(TriggerPin, LOW);  //para generar un pulso limpio ponemos a LOW 4us
  delayMicroseconds(4);
  digitalWrite(TriggerPin, HIGH);  //generamos Trigger (disparo) de 10us
  delayMicroseconds(10);
  digitalWrite(TriggerPin, LOW);

  duration = pulseIn(EchoPin, HIGH);  //medimos el tiempo entre pulsos, en microsegundos

  distanceCm = duration * 10 / 292 / 2;  //convertimos a distancia, en cm
  return distanceCm;
}


void loop() {
  hum = dht.readHumidity();
  tempe= dht.readTemperature();
  Irms = energyMonitor.calcIrms(1484);
  potencia =  Irms * voltajeRed;
  water=ping(4,13);
  
  tiempo_act= millis();
  if (tiempo_act - tiempo_ant <= 5000 && !sondaTomoMedida) {
      //Control de 5v con arduino
      digitalWrite(9,HIGH);
      //Control de tierra con rele
      digitalWrite(10,HIGH);
      //Control de 5v con Arduino para nivel de agua
      digitalWrite(13,HIGH);
      sonda();
      sondaTomoMedida=true;
      
  }
    //
    //Serial.println(tiempo_act);
    //Serial.println(tiempo_ant);
    //Serial.println(tiempo_act - tiempo_ant);
    
      
  //PH
   if (tiempo_act - tiempo_ant > 5000 && tiempo_act - tiempo_ant <= 9000 && !phTomoMedida) {
      //Control de 5v con arduino
      digitalWrite(9,LOW);
      //Control de tierra con rele
      digitalWrite(10,LOW);
      delay(500);
      //Control de 5v con Arduino para nivel de agua
      digitalWrite(13,HIGH);
      medirPH();
      phTomoMedida=true;
    } 

   if (tiempo_act - tiempo_ant > 9000 && tiempo_act - tiempo_ant <= 10000 && !restoTomoMedida) {
      
      //Control de 5v con arduino
      digitalWrite(9,HIGH);
      //Control de tierra con rele
      digitalWrite(10,HIGH);
      //Control de 5v con Arduino para nivel de agua
      //digitalWrite(13,LOW);
      
      
     
      
      //digitalWrite(9,LOW);
   
    // Calculamos la potencia aparente
      
      restoTomoMedida=true;
   }
   
    
  if(tiempo_act - tiempo_mensaje >= 2000) {
    tiempo_mensaje=tiempo_act;
    
    doc["nitrogeno"]=nitrogeno;
    doc["fosforo"]=phosforo;
    doc["potasio"]=potasio;
    doc["humedad_ext"]=hum;
    doc["temperatura_ext"]=tempe;
    doc["irms"]=Irms;
    doc["potencia"]=potencia;
    doc["ph"]=phValue;
    doc["agua"]=water;
    //serializeJsonPretty(doc, Serial);
  }
    
  if (tiempo_act - tiempo_ant >= 10000) {
    phTomoMedida=false;
    sondaTomoMedida=false;
    restoTomoMedida=false;
    tiempo_ant=tiempo_act;
    }
  readSerialPort();
  
  if (ser == "D") {
    serializeJson(doc, Serial);
    
  }
}

void sonda(){
    nitrogeno = nitrogen();
    delay(250);
    //erial.println(nitrogeno);
    phosforo = phosphorous();
    delay(250);
    //Serial.println(phosforo);
    potasio = potassium();
    delay(250);
    //Serial.println(potasio);
  
  }
 
byte nitrogen(){
  digitalWrite(DE,HIGH);
  digitalWrite(RE,HIGH);
  delay(10);
  if(mod.write(nitro,sizeof(nitro))==8){
    digitalWrite(DE,LOW);
    digitalWrite(RE,LOW);
    for(byte i=0;i<7;i++){
    //Serial.print(mod.read(),HEX);
    values[i] = mod.read();
    //Serial.print(values[i],HEX);
    }
    //Serial.println();
  }
  return values[4];
}
 
byte phosphorous(){
  digitalWrite(DE,HIGH);
  digitalWrite(RE,HIGH);
  delay(10);
  if(mod.write(phos,sizeof(phos))==8){
    digitalWrite(DE,LOW);
    digitalWrite(RE,LOW);
    for(byte i=0;i<7;i++){
    //Serial.print(mod.read(),HEX);
    values[i] = mod.read();
    //Serial.print(values[i],HEX);
    }
    //Serial.println();
  }
  return values[4];
}
 
byte potassium(){
  digitalWrite(DE,HIGH);
  digitalWrite(RE,HIGH);
  delay(10);
  if(mod.write(pota,sizeof(pota))==8){
    digitalWrite(DE,LOW);
    digitalWrite(RE,LOW);
    for(byte i=0;i<7;i++){
    //Serial.print(mod.read(),HEX);
    values[i] = mod.read();
    //Serial.print(values[i],HEX);
    }
    //Serial.println();
  }
  return values[4];
}

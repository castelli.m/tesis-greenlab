
<script type="text/javascript">
    function contarNotis(){
            var cantidadActual=0; 
            var cantidad=0;
            {% if notificaciones_list%}
            {% for noti in notificaciones_list%}
                {%if noti.mostrar%}
                cantidad=cantidad+1;
                {%endif%}
            {% endfor %}
            {% endif %}
         return cantidad; 
         }  

  function noMostrarNoti(){
            
            var idNotificacion=document.getElementById('idNoMostrarNoti').value;
             $.ajax({
                    type:"GET",
                    contentType:"text; charset=utf-8",
                    dataType:"text",
                    url:"/ws/noMostrarNoti/"+idNotificacion,
                    success:function(response){
                        if (response=="ok") {

                        } else{

                        };
                    }
                });


        }

        

</script>
<script type="text/javascript">
    function cargarSensoresModal(idHabitacion){
    
                $.ajax({
                    type:"GET",
                    contentType:"text; charset=utf-8",
                    dataType:"text",
                    url:"/ws/listaSensores/"+idHabitacion,
                    success:function(response){
                         var sensores=JSON.parse(response);
                         var valor="";
                         $('#accesos1').append("<div class='row'>");
                         if (sensores['sensores'].length==0) {
                             $('#accesos1').append("<h3><p align='left'>No hay sensores.</p></h3>");
                         } else{
                         $('#accesos1').append("<div><h3><p align='left'>Sensores:</p></h3>");
                            for (var i = 0; i<= sensores['sensores'].length-1; i++) {
                                valor=sensores['sensores'][i]['valor'];
                                if(sensores['sensores'][i]['aliasSensor'] =="V"){
                                    if(sensores['sensores'][i]['valor']==0){
                                        valor="Cerrada";
                                    }else{
                                        valor="Abierta";    
                                    }
                                }
                               
                                 $('#accesos1').append("<div class='col-lg-4 col-md-6'><div id='sensor"+sensores['sensores'][i]['idSensor']+"' class='panel panel-gris2'><div class='panel-heading'><div class='row'><div class='col-xs-5'><img class='img-responsive'  src='"+sensores['sensores'][i]['imagen']+"'></div><div class='col-xs-7 text-right'><div ><!--{{sensor.tipoSensor.alias}}{{sensor.id_arduino}}=-->"+valor+" "+sensores['sensores'][i]['unidadSensor']+"</div></div></div></div><div class='panel-footer'><span class='pull-left'>"+sensores['sensores'][i]['descripcion']+"</span></i></span><div class='clearfix'></div></div></div></div></div>");
                            

                             }
                             $('#accesos1').append("</div>");

                             };

                             //cargarActuadoresModal(idHabitacion);
                          }
                        }); 
                        
                       

        } 
   


</script>

<script type="text/javascript">

    function cargarActuadoresModal(idHabitacion){
    
                $.ajax({
                    type:"GET",
                    contentType:"text; charset=utf-8",
                    dataType:"text",
                    url:"/ws/listaActuadores/"+idHabitacion,
                    success:function(response){
                         var actuadores=JSON.parse(response);
                         $('#accesos2').append("<div class='row'>");
                        if ((actuadores['actuadores'].length)==0) {
                             $('#accesos2').append("<h3><p align='left'>No hay actuadores.</p></h3>");
                         } else{
                            $('#accesos2').append("<div><h3><p align='left'>Actuadores:</p></h3>");
                        
                            for (var i = 0; i<= actuadores['actuadores'].length-1; i++) {

                               


                                 $('#accesos2').append("<div class='col-lg-4 col-md-6'><div id='sensor"+actuadores['actuadores'][i]['idActuador']+"' class='panel panel-rojo'><div class='panel-heading'><div class='row'><div class='col-xs-5'><a href='javascript:cambiarEstadoLuz("+actuadores['actuadores'][i]['idActuador']+")''><img class='img-responsive'  src='"+actuadores['actuadores'][i]['imagen']+"'></a></div><div class='col-xs-7 text-right'><div ><!--{{sensor.tipoSensor.alias}}{{sensor.id_arduino}}=--></div></div></div></div><div class='panel-footer'><span class='pull-left'>"+actuadores['actuadores'][i]['descripcion']+"</span></i></span><div class='clearfix'></div></div></div></div></div>");
                            

                             }
                             $('#accesos2').append("</div>");
                         };

                          }
                        }); 
  
        } 
       

</script>

<script type="text/javascript">
  function desarrollarModal(idNotificacion){
         $('#mensajeria').hide();
         $.ajax({
                    type:"GET",
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    url:"/ws/notificaciones/"+idNotificacion,
                    success:function(response){
                        var notificacion=JSON.parse(response);
                        
                          //$('#texto').children().remove();
                          document.getElementById('idNoMostrarNoti').value=idNotificacion;
                          $('#texto').children().remove();
                          $('#texto').append("<p>"+ notificacion[0]['fields']['mensaje']+"</p>")
                          $('#myModal').modal(); 
                          $('#btnEnviar').hide();
                          $('#btnCerrar2').hide();
                          $('#btnVisto').show();
                          $('#btnCerrar1').show();

                        }
                    });

          
        }  

</script>


 <script type="text/javascript">
    function crearNotificacion(){
                        $('#texto').children().remove();
                        $('#mensajeria').show();
                        $('#myModal').modal();
                        $('#btnEnviar').show();
                        $('#btnCerrar2').show();
                        $('#btnVisto').hide();
                        $('#btnCerrar1').hide(); 
     } 

     function crearAccesos(habitacion){
                        //$('#txtHabitacion').value=habitacion;
                        $('#accesos1').children().remove();
                        $('#accesos2').children().remove();
                        cargarSensoresModal(habitacion);
                        cargarActuadoresModal(habitacion);
                        $('#myModalAccesos').modal();
                        
     }  
    
    function enviarMensaje(){
               var mensaje=document.getElementById("mensaje").value;    
               $.ajax({
                          type: "POST",
                          dataType:"json",
                          url: "/sistema/addNotificacion/",
                          data: {"mensaje":mensaje},
                          success: function(data) {
                                //$('#idNotificacionesUl').children().remove();
                                $('#myModal').modal('hide');
                          }
                        });           
                        

        }  

     function verMensajes(){
      
         $.ajax({
                    type:"GET",
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    url:"/ws/notificaciones",
                    success:function(response){
                        var notificacion=JSON.parse(response);
                        
                         
                        if (notificacion.length!=0) {

                          $('#contenedorMensajes').children().remove();
                            for (var i = 0; i<= notificacion.length-1; i++) {
                                
                                    
                                    $('#contenedorMensajes').append("<a href='javascript:desarrollarModal("+notificacion[i]['pk']+")' class='list-group-item ' id="+'pk-'+notificacion[i]['pk']+"><div ><i class='fa fa-comment fa-fw'></i><strong>"+notificacion[i]['fields']['usuario']+"</strong><span class='pull-right text-muted'><em>"+notificacion[i]['fields']['fecha']+"</em></span></div><div> "+ notificacion[i]['fields']['mensaje'] +"</div></a> <input type='text' value="+notificacion[i]['pk']+" id="+'noti-'+notificacion[i]['pk']+" hidden >");
                                              
                                   
                                    
                                
                         }
                         $('#myModalMensajes').modal(); 
                        }
                      }
                    });
 
                        

        }  
      

	
    function cambiarEstadoLuz(idInterruptor){
                //var estado= $("switch-state-"+idInterruptor).value;
                var imgSrc="";
              
                        $.ajax({
                          type: "POST",
                          dataType:"text",
                          url: "/ws/cambiarEstadoInterruptor/",
                          data: {"idInterruptor":idInterruptor},
                          success: function(dato) {
                                
                                if(dato=="-1"){
                                    alert("Esta desconectado");
                                }else{    
                                     $("#img"+idInterruptor).attr("src",dato);
                                     $("switch-state-"+idInterruptor).value=!estado;
                                }
                         }
                        }); 
                         $('#myModalAccesos').modal('hide');

       
        } 


   function simularPresencia(estado){
		 $.ajax({
                    type:"GET",
                    contentType:"text; charset=utf-8",
                    dataType:"text",
                    url:"/ws/simularPresencia/"+estado,
                    success:function(response){
								//alert(response);
                                if(response=="-1"){
                                    alert("Hubo un error!");
                                }else if(response=="true"){    
                                     alert("La simulación ha comenzado!");
                                }else {
									alert("La simulación ha finalizado!");
									}
                    }
                });
                                
                               
                          
		}

</script>

<script type="text/javascript">
    function logout(){
            var txt;
            var r = confirm("Esta seguro que desea salir?");
            if (r == true) {
                 $.ajax({
                    type:"GET",
                    contentType:"text; charset=utf-8",
                    dataType:"text",
                    url:"/accounts/logout/",
                    success:function(response){
                        if (response=="ok") {
                            
                        } else{

                        };
                    }
                });
            } else {
             

            }
               

            

        } 

  
</script>
<style type="text/css">
  .modal-body {
  max-height: calc(100vh - 212px);
  overflow:auto;
}
</style>

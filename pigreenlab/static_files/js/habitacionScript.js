

   
 <script type="text/javascript">
 
 function cambiarEstadoLuz(idInterruptor){
                var estado= $("switch-state-"+idInterruptor).value;
                var dato="";
              
                        $.ajax({
                          type: "POST",
                          dataType:"text",
                          url: "/ws/cambiarEstadoInterruptor/",
                          data: {"idInterruptor":idInterruptor},
                          success: function(dato) {
                                if(dato=="-1"){
                                    alert("Esta desconectado");
                                }else{    
                                     $("#img"+idInterruptor).attr("src",dato);
                                     $("switch-state-"+idInterruptor).value=!estado;
                                }
                          }
                        }); 
       
        } 

        function retornarNombreTipoSensor(idTipo){
             $.ajax({
                     type:"GET",
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    url:"/ws/tipoSensores/"+idTipo,
                    success:function(response){
                        var mensajep=JSON.parse(response);
                        var retorno=mensajep[i]['fields']['alias'];
                        return retorno;
                        
                    }
                });


        }
  
  function cambiarImgSensor(idSensor){
             $.ajax({
                    type:"GET",
                    contentType:"application/json; charset=utf-8",
                    dataType:"text",
                    url:"/ws/sensores/"+idSensor,
                    success:function(url){
								// alert(url);
                        		$('#imgSensor'+idSensor).children().remove();
								$('#imgSensor'+idSensor).append("<img class='img-responsive' src='"+url+"'>");
          
                        
                    }
                });


        }
  
       
 
</script>
<script type="text/javascript">
            function noMostrarNoti(){
            
            var idNotificacion=document.getElementById('idNoMostrarNoti').value;
            //alert('prueba');
            //alert(idNotificacion);
             $.ajax({
                    type:"GET",
                    contentType:"text; charset=utf-8",
                    dataType:"text",
                    url:"/ws/noMostrarNoti/"+idNotificacion,
                    success:function(response){
                        alert('paso');  
                        if (response=="ok") {

                        } else{

                        };
                    }
                });


        }

             function updateSensores(){
               var valor="";
               var habitacion=document.getElementById('idHabitacion').value;
               //alert("prueba");
                $.ajax({
                    type:"GET",
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    url:"/ws/actualizarSensores/"+habitacion,
                    success:function(response){
					var unidad="";
						
                        if (response['sensores'].length>0) {
                           //alert(response['sensores']); 
                            //alert(response['sensores'][0]['valor']);
                            for (var i = 0; i < response['sensores'].length; i++) {
								unidad=response['sensores'][i]['unidadSensor'];
								if(unidad=="-"){
									unidad="";
								}else if(unidad=="."){
									unidad="";
								}
                                //alert('entroooo');
                                valor=response['sensores'][i]['valor'];
                                if(response['sensores'][i]['aliasSensor'] =="Va" || response['sensores'][i]['aliasSensor'] =="Vc"){
                                    cambiarImgSensor(response['sensores'][i]['idSensor']);
                                    if(response['sensores'][i]['valor']==0){
                                        valor="Cerrada";
                                    }else{
                                        valor="Abierta";    
                                    }
                                }else if(response['sensores'][i]['aliasSensor'] =="R"){
									if(response['sensores'][i]['valor']==0){
                                        valor="No llueve";
                                    }else{
                                        valor="Lueve";    
                                    }
								}else if(response['sensores'][i]['aliasSensor'] =="X"){
									if(response['sensores'][i]['valor']<=22 || response['sensores'][i]['valor']>=337){
                                        valor="Norte";
                                    }else if(response['sensores'][i]['valor']>=22 && response['sensores'][i]['valor']<67){
                                        valor="Noreste";
                                    }else if(response['sensores'][i]['valor']>=67 && response['sensores'][i]['valor']<112){
                                        valor="Este";
                                    }else if(response['sensores'][i]['valor']>=122 && response['sensores'][i]['valor']<157){
                                        valor="Sureste";
                                    }else if(response['sensores'][i]['valor']>=157 && response['sensores'][i]['valor']<203){
                                        valor="Sur";
                                    }else if(response['sensores'][i]['valor']>=203 && response['sensores'][i]['valor']<247){
                                        valor="Suroeste";
                                    }else if(response['sensores'][i]['valor']>=247 && response['sensores'][i]['valor']<292){
                                        valor="Oeste";
                                    }else if(response['sensores'][i]['valor']>=292 && response['sensores'][i]['valor']<337){
                                        valor="Noroeste";
                                    }
								}
                                $('#text'+response['sensores'][i]['idSensor']).children().remove();
                                $('#text'+response['sensores'][i]['idSensor']).append("<div id='sens'"+response['sensores'][i]['idSensor']+">"+valor+unidad+"</div>");
								
								
								
								
								 };
                        } else{
                            
                        };

                    }
                       
                });

                }


             function updateActuadores(){
               var habitacion=document.getElementById('idHabitacion').value;
                $.ajax({
                    type:"GET",
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    url:"/ws/listaActuadores/"+habitacion,
                    success:function(response){
                      var boton ="";

                        if (response['actuadores'].length>0) {
                           
                            for (var i = 0; i < response['actuadores'].length; i++) {
                          
                                $('#imgActuador'+response['actuadores'][i]['idActuador']).children().remove();
                                $('#imgActuador'+response['actuadores'][i]['idActuador']).append("<a href='javascript:cambiarEstadoLuz("+response['actuadores'][i]['idActuador']+")'><img class='img-responsive' src='"+ response['actuadores'][i]['imagen'] +"' id='img"+response['actuadores'][i]['idActuador']+"'></a>");
                               
                             
                            };
                          
                        } else{
                            
                        };

                    }
                       
                });

                }
      
             

                          function update(){
               //prueba=document.getElementById('sonido').play();
               //$('#sonido')[0].play();
                $.ajax({
                    type:"GET",
                    contentType:"application/json; charset=utf-8",
                    dataType:"json",
                    url:"/ws/notificaciones/",
                    success:function(response){
                        var hora="";
                        var mensajep=JSON.parse(response);
                        var valor=document.getElementById('contadorNotis').value;
                        var cantMensajes=0;

                        
                        
                         


                         

                        if (valor!=mensajep.length & valor!=0) {
                          
                            $('#idNotificacionesUl').children().remove();
                             $('#idNotificacionesUl').append("<li class='message-footer' id='todosMensajes'><a href='javascript:verMensajes()'>Leer Todos los mensajes</a></li><li class='message-footer' id='crearMensaje'><a href='javascript:crearNotificacion()'>Crear Mensaje</a></li>");
                            var cnotis=0;
                            
                         
                            for (var i = 0; i<= mensajep.length-1; i++) {
                                hora=mensajep[i]['fields']['fecha'].split(' ')[1].substring(0,5);
                                                               cnotis=cnotis+1;
                                if(mensajep[i]['fields']['mensaje']!=undefined){

                                 if(mensajep[i]['fields']['mensaje'].length>30){
                                    texto=mensajep[i]['fields']['mensaje'].substring(0,20)+"...";
                                 }else{
                                    texto=mensajep[i]['fields']['mensaje'];
                                 }
                                
                             
                                   $('#idNotificacionesUl').append("<li class='message-preview'><a href='javascript:desarrollarModal("+mensajep[i]['pk']+")'><div class='media'><span class='pull-left'><img class='media-object' src='http://placehold.it/50x50' alt=''></span><div class='media-body'><h5 class='media-heading'><strong>"+mensajep[i]['fields']['usuario']+"</strong></h5><p class='small text-muted'><i class='fa fa-clock-o'></i> "+hora+"</p><p>"+ texto +"</p></div></div></a></li><input type='text' value="+mensajep[i]['pk']+" id="+'noti-'+mensajep[i]['pk']+" hidden >");

                                    
                         
                                 }
                            };
                            var valor=document.getElementById('contadorNotis').value;
                            if(valor<cnotis){
                                 // alert('Tiene que sonar');
                                 //document.getElementById('sonido').play();
                                 document.getElementById('btn').click();                                 
                                 document.getElementById('contadorNotis').value=cnotis;
                            
                             }else{
                                
                                 document.getElementById('contadorNotis').value=cnotis;
                             }

                            
                        }else if(valor==0){
                            $('#idNotificacionesUl').children().remove();
                             $('#idNotificacionesUl').append("<li class='message-footer' id='todosMensajes'><a href='javascript:verMensajes()'>Leer Todos los mensajes</a></li><li class='message-footer' id='crearMensaje'><a href='javascript:crearNotificacion()'>Crear Mensaje</a></li>");
                            var cnotis=0;
                            //if(mensajep.length-1<5){cantMensajes=mensajep.length-1}
                             // else{cantMensajes=5}
                            for (var i = 0; i<= mensajep.length-1; i++) {
                                hora=mensajep[i]['fields']['fecha'].split(' ')[1].substring(0,5);
                                cnotis=cnotis+1;
                                if(mensajep[i]['fields']['mensaje']!=undefined){

                       
                            
                             if(mensajep[i]['fields']['mensaje'].length>30){
                                texto=mensajep[i]['fields']['mensaje'].substring(0,20)+"...";
                             }else{
                                texto=mensajep[i]['fields']['mensaje'];
                             }
                                                              
                                $('#idNotificacionesUl').append("<li class='message-preview'><a href='javascript:desarrollarModal("+mensajep[i]['pk']+")'><div class='media'><span class='pull-left'><img class='media-object' src='http://placehold.it/50x50' alt=''></span><div class='media-body'><h5 class='media-heading'><strong>"+mensajep[i]['fields']['usuario']+"</strong></h5><p class='small text-muted'><i class='fa fa-clock-o'></i> "+hora+"</p><p>"+ texto +"</p></div></div></a></li><input type='text' value="+mensajep[i]['pk']+" id="+'noti-'+mensajep[i]['pk']+" hidden >");
  
                             
                                }
                            };
                             var valor=document.getElementById('contadorNotis').value;
                             document.getElementById('contadorNotis').value=cnotis;
                        
                             
                        }

                    }
                });

                }


            

        function refresh(){
            location.reload();
        }


        
            
        </script>  


          <script type="text/javascript">
            $(document).on('ready',function(){ 


            function updateAll(){
                //alert('entro al updateAll()');
                update();
                //alert('Actualizo Notificaciones');
                updateActuadores();
                //alert('Actualizo Actuadores');
                updateSensores();
                //alert('Actualizo Sensores');
                
                
                


            }

         setInterval(updateAll, 2000);
         
    });
            </script>
        <script type="text/javascript">

            

            function cargarVideo(){
                 urlCorrecta=obtenerUrl();
                
               // window.frames['video'].location="http://camaracerebrito.dlinkddns.com/video.cgi";

                window.frames['video'].location="http://admin:getsmart@192.168.0.103/mjpeg.cgi";

            }

            function noVerVideo(){
                      $('#videoDiv').children().remove();
                           

            }
            function verVideo(){
                    urlCorrecta=obtenerUrl();
                     $('#videoDiv').children().remove();
                     $('#videoDiv').append("<iframe name='video' id='video' width='100%' height='100%'>Video</iframe>");
                     cargarVideo("urlCorrecta");

                    
                }
            function obtenerUrl(){
                str=document.getElementById("idUrl").value;
                array=str.split("-");
                return array[2].split(">")[0];
            }
        
        </script>

from django.shortcuts import render
from django.http import HttpResponse
from greenLab.models import Sensor,TipoSensor, Actuador
from django.core import serializers
import json
from django.db.models import Q
import decimal, simplejson
from decimal import Decimal

def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return float(obj)
    raise TypeError
# Create your views here.



def wsSensores(request):
	data = serializers.serialize("json",Sensor.objects.all())
	return HttpResponse(
                json.dumps(data), content_type='application/json'
    )


def wsActuadores(request):
	data = serializers.serialize("json",Actuador.objects.all())
	return HttpResponse(
                json.dumps(data), content_type='application/json'
    )


def wsUnidad(request,idTipo):
	data = serializers.serialize("json",TipoSensor.objects.filter(pk=idTipo))
	return HttpResponse(
                json.dumps(data), content_type='application/json'
    )
 



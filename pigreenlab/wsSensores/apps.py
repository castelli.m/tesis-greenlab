from django.apps import AppConfig


class WssensoresConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wsSensores'

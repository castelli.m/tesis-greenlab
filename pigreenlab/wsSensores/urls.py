from django.urls import path
from . import views

urlpatterns = [
    path('', views.wsSensores, name="index"),
    path('unidad/<int:idTipo>', views.wsUnidad),
    path('actuadores/', views.wsActuadores),
 
]

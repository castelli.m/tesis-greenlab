from django.contrib import admin

# Register your models here.
from .models import Sensor,TipoSensor,Actuador,EstadoActuador,Condicion,Controlador,Camara,EstadoAmbiente

admin.site.site_header = "GreenLab"
admin.site.site_title = "Portal de Gestion"

class SensorAdmin(admin.ModelAdmin):
		list_display = ("descripcion","tipoSensor","valor")
		list_filter = ('descripcion','tipoSensor')
		ordering = ('descripcion',)
		search_fields = ('descripcion',)

admin.site.register(Sensor,SensorAdmin)

class TipoSensorAdmin(admin.ModelAdmin):
		list_display = ("descripcion","tipo","alias")
		list_filter = ('descripcion','tipo','alias')
		ordering = ('descripcion',)
		search_fields = ('descripcion',)

admin.site.register(TipoSensor,TipoSensorAdmin)

class CamaraAdmin(admin.ModelAdmin):
		list_display = ("url",)
		list_filter = ('url',)
		

admin.site.register(Camara,CamaraAdmin)

class ControladorAdmin(admin.ModelAdmin):
		list_display = ("descripcion","mac","descripcion_estado")
		list_filter = ('descripcion','mac','descripcion_estado')
		ordering = ('descripcion',)
		search_fields = ('descripcion',)

admin.site.register(Controlador,ControladorAdmin)


class ActuadorAdmin(admin.ModelAdmin):
		list_display = ("descripcion","controlador","valor")
		list_filter = ('descripcion','controlador','valor')
		ordering = ('descripcion',)
		search_fields = ('descripcion',)

admin.site.register(Actuador,ActuadorAdmin)

admin.site.register(Condicion)

class EstadoActuadorAdmin(admin.ModelAdmin):
		list_display = ("actuador","estado")
		list_filter = ('actuador','estado')
		ordering = ('actuador',)
		search_fields = ('actuador',)

admin.site.register(EstadoActuador,EstadoActuadorAdmin)

class EstadoAmbienteAdmin(admin.ModelAdmin):
		list_display=("temperaturaInterior","temperaturaExterior","humedadInterior","humedadExterior","co2","fecha")
		ordering = ('fecha',)
admin.site.register(EstadoAmbiente,EstadoAmbienteAdmin)


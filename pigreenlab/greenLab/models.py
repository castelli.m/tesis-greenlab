from django.db import models
from django.db.models import signals
import sys
import random
from datetime import datetime
from datetime import timedelta
from django.utils import timezone
import subprocess
import time
from django.db.models.signals import post_save
from django.dispatch import receiver

import paho.mqtt.client as mqtt
# Create your models here.


class EstadoAmbiente(models.Model):
    ESTADOS = ((1, 'ON'), (0, 'OFF'),)
    # Datos Descriptivos
    fecha = models.DateTimeField(default=datetime.now())
    # Estado de los sensores
    temperaturaInterior =models.FloatField(default = "0")
    temperaturaExterior = models.FloatField(default="0", max_length=3)
    humedadInterior = models.FloatField(default="0")
    humedadExterior = models.FloatField(default="0")
    luminosidadInterior = models.IntegerField(default="0", max_length=5)
    co2 = models.IntegerField(default="0", max_length=5)
    amoniaco = models.FloatField(default="0")
    co = models.FloatField(default="0")
    no2 = models.FloatField(default="0")
    propano = models.FloatField(default="0")
    butano = models.FloatField(default="0")
    metano = models.FloatField(default="0")
    hidrogeno = models.FloatField(default="0")
    etanol = models.FloatField(default="0")
    distancia = models.IntegerField(default="0", max_length=5)
    puerta = models.IntegerField(default="0", max_length=1)
    nitrogeno = models.IntegerField(default="0", max_length=5)
    fosforo = models.IntegerField(default="0", max_length=5)
    potasio = models.IntegerField(default="0", max_length=5)
    irms = models.FloatField(default="0")
    potencia = models.FloatField(default="0")
    ph = models.FloatField(default="0")
    nivelAgua = models.IntegerField(default="0", max_length=5)
    imagen = models.CharField(default="abstract", max_length=100)
    # Estado de los actuadores

    caloDerecho = models.IntegerField(default=0, choices=ESTADOS)
    caloIzquierdo = models.IntegerField(default=0, choices=ESTADOS)
    frioDerecho = models.IntegerField(default=0, choices=ESTADOS)
    frioIzquierdo = models.IntegerField(default=0, choices=ESTADOS)
    luzDerecha = models.IntegerField(default=0, choices=ESTADOS)
    luzIzquierda = models.IntegerField(default=0, choices=ESTADOS)
    luzCentral = models.IntegerField(default=0, choices=ESTADOS)
    ventilacionDerecha = models.IntegerField(default=0, choices=ESTADOS)
    ventilacionIzquierda = models.IntegerField(default=0, choices=ESTADOS)
    bombaAgua = models.IntegerField(default=0, choices=ESTADOS)
    peristalticaAcido = models.IntegerField(default=0, choices=ESTADOS)
    peristalticaBase = models.IntegerField(default=0, choices=ESTADOS)
    peristalticaNitro = models.IntegerField(default=0, choices=ESTADOS)
    peristalticaPota = models.IntegerField(default=0, choices=ESTADOS)
    peristalticaFosf = models.IntegerField(default=0, choices=ESTADOS)
    camara = models.IntegerField(default=0, choices=ESTADOS)

    class Meta:
        verbose_name = 'Estado Ambiente'
        verbose_name_plural = 'Estados Ambiente'

    def __str__(self):
        return str(self.fecha)

    def __unicode__(self):
        return str(self.fecha)


class Camara(models.Model):
    url = models.CharField(default="abstract", max_length=100)

    class Meta:
        verbose_name = 'Camara'
        verbose_name_plural = 'Camaras'

    def __str__(self):
        return self.url

    def __unicode__(self):
        return self.url


class Controlador(models.Model):

    descripcion = models.CharField(default="Sin descripcion", max_length=50)
    mac = models.CharField(max_length=50, null=True)
    descripcion_estado = models.CharField(
        default="Sin descripcion", max_length=100)
    reseteo = models.BooleanField(default=False)
    fuerza_senal = models.IntegerField(default="0", max_length=50)
    ultimo_cambio = models.DateTimeField(null=True, blank=True)
    debug = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Controlador'
        verbose_name_plural = 'Controladores'

    def __str__(self):
        return self.descripcion

    def __unicode__(self):
        return self.descripcion


class TipoSensor(models.Model):
    imagen = models.ImageField(null=True,upload_to='static/imagenes')
    descripcion = models.CharField(default="abstract", max_length=100)
    tipo = models.CharField(default="", max_length=50)
    alias = models.CharField(default="", max_length=50)
    unidad = models.CharField(default="", max_length=50)
    color= models.CharField(default="", max_length=50)
    valorMaximo = models.DecimalField(
        default="100", max_digits=8, decimal_places=2)
    valorMinimo = models.DecimalField(
        default="0", max_digits=8, decimal_places=2)

    class Meta:
        verbose_name = 'Tipo de Sensor'
        verbose_name_plural = 'Tipos de Sensores'

    def __str__(self):
        return self.descripcion

    def __unicode__(self):
        return self.tipo


class Sensor(models.Model):
    tipoSensor = models.ForeignKey(TipoSensor, on_delete=models.CASCADE)
    descripcion = models.CharField(default="Sin descripcion", max_length=50)
    orden =  models.CharField(default="1", max_length=1)
    controlador = models.ForeignKey(
        Controlador, null=True, blank=True, on_delete=models.CASCADE)
    # Valor del Sensor
    valor = models.FloatField(default="0")
    factorCorreccion = models.DecimalField(
        default="0", max_digits=8, decimal_places=2)
    factorMultiplicacion = models.DecimalField(
        default="1", max_digits=8, decimal_places=2)
    ultimo_cambio = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = 'Sensor'
        verbose_name_plural = 'Sensores'

    def __unicode__(self):
        return self.descripcion

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        
        self.ultimo_cambio = timezone.now()

        super(Sensor, self).save(*args, **kwargs)


class Actuador(models.Model):
    descripcion = models.CharField(default="Sin descripcion", max_length=50)
    controlador = models.ForeignKey(
        Controlador, null=True, on_delete=models.CASCADE)
    tipoActuador = models.ForeignKey(TipoSensor, on_delete=models.CASCADE)
    id_rele = models.IntegerField(max_length=50)
    # Valor del actuador, 1 = prendido y 0 = apagado
    PRENDIDO = 1
    APAGADO = 0
    TIPO_CHOICES_2 = (
        (PRENDIDO, 'Prendido'),
        (APAGADO, 'Apagado'),
    )
    valor = models.IntegerField(
        default="0", max_length=50, choices=TIPO_CHOICES_2)
    estado_verificado = models.NullBooleanField(default=False)

    def __unicode__(self):
        return self.descripcion

    def __str__(self):
        return self.descripcion

    def save(self, *args, **kwargs):
        
        #self.valor = (self.valor*self.factorMultiplicacion) + \
            #self.factorCorreccion
        super(Actuador, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'Actuador'
        verbose_name_plural = 'Actuadores'

    def __unicode__(self):
        return self.descripcion


class Condicion(models.Model):
    sensor = models.ForeignKey(Sensor, on_delete=models.CASCADE)
    sensor2 = models.ForeignKey(
        Sensor, on_delete=models.CASCADE, blank=True, null=True, related_name='sensor2')
    CONDICIONES_CHOICES = (("<", "<"), (">", ">"), ("==", "=="),
                           ("!=", "!="), ("<=", "<="), (">=", ">="))
    condicion = models.CharField(max_length=2,
                                 choices=CONDICIONES_CHOICES,
                                 default="==")
    #comparar = models.IntegerField(default="0", max_length=50)
    comparar = models.DecimalField(default="0", max_digits=8, decimal_places=1)

    class Meta:
        verbose_name = 'Condicion'
        verbose_name_plural = 'Condiciones'

    def __unicode__(self):
        return self.sensor.descripcion+self.condicion+unicode(self.comparar)

    def evaluar(self):
        if self.sensor2 is None:
            op = cmp(self.sensor.valor, self.comparar)
            if ((self.condicion == "<" or self.condicion == "<=") and op < 0):
                return True
            elif ((self.condicion == "==" or self.condicion == "<=" or self.condicion == ">=") and op == 0):
                return True
            elif ((self.condicion == ">" or self.condicion == ">=") and op > 0):
                return True
            else:
                return False
        else:
            valor_a_comparar = self.sensor2.valor + self.comparar
            op = cmp(self.sensor.valor, valor_a_comparar)
            if ((self.condicion == "<" or self.condicion == "<=") and op < 0):
                return True
            elif ((self.condicion == "==" or self.condicion == "<=" or self.condicion == ">=") and op == 0):
                return True
            elif ((self.condicion == ">" or self.condicion == ">=") and op > 0):
                return True
            else:
                return False


class EstadoActuador(models.Model):
    actuador = models.ForeignKey(Actuador, on_delete=models.CASCADE)

    # Valor del actuador, 1 = prendido y 0 = apagado
    PRENDIDO = 1
    APAGADO = 0
    TIPO_CHOICES_2 = (
        (PRENDIDO, 'Prendido'),
        (APAGADO, 'Apagado'),
    )

    estado = models.IntegerField(
        default="0", max_length=50, choices=TIPO_CHOICES_2)

    class Meta:
        verbose_name = 'Estado Actuador'
        verbose_name_plural = 'Estado Actuadores'

    def __str__(self):
        return self.actuador.descripcion+"="+unicode(self.estado)

    def __unicode__(self):
        return self.actuador.descripcion+"="+unicode(self.estado)

    def ejecutar(self):
        if(self.actuador.valor != self.estado):
            self.actuador.valor = self.estado
            self.actuador.save()
            
@receiver(post_save, sender=Actuador, dispatch_uid="update_act")
def update_act(sender, instance, **kwargs):
        client = mqtt.Client() 
        client.connect('localhost', 1883, 60) 
        # Connect to the MQTT server and process messages in a background thread. 
        client.loop_start() 
        topico=instance.controlador.mac       
        if(topico=="b4:e6:2d:00:ee:bc"):
            client.publish(topico, instance.valor)
            
        else:
            if(instance.id_rele==0 and instance.valor==0):
                client.publish(topico, 0)
            elif (instance.id_rele==0 and instance.valor==1):
                client.publish(topico, 1)
            elif (instance.id_rele==1 and instance.valor==0):
                client.publish(topico, 2)
            elif (instance.id_rele==1 and instance.valor==1):
                client.publish(topico, 3)

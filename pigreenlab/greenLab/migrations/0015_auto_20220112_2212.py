# Generated by Django 3.2.7 on 2022-01-12 22:12

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('greenLab', '0014_alter_estadoambiente_fecha'),
    ]

    operations = [
        migrations.AlterField(
            model_name='estadoambiente',
            name='fecha',
            field=models.DateTimeField(default=datetime.datetime(2022, 1, 12, 22, 11, 59, 707227)),
        ),
        migrations.AlterField(
            model_name='tiposensor',
            name='imagen',
            field=models.ImageField(null=True, upload_to='imagenes'),
        ),
    ]

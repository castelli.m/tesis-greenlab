from django.core.management.base import BaseCommand, CommandError
from greenLab.models import EstadoAmbiente as EA,Sensor
import numpy as np
import random
from gym import Env

import time
import datetime
import RPi.GPIO as GPIO
import paho.mqtt.client as mqtt
import serial
from math import exp

import json
# import requests
diccionarioSensores = dict()
diccionarioSensores2 = dict()
vacio = dict()

tempInicial = 22
tiempoStep = 120
tmin = float('26')
tmax = float('32')
CaloRefriI = "b4:e6:2d:03:c2:42"
RefriCaloD = "b4:e6:2d:03:45:d4"
Ventilar = "b4:e6:2d:03:ac:01"

entro = 0
arduino = serial.Serial("/dev/ttyACM0", baudrate=9600)
arduino2 = serial.Serial("/dev/ttyACM1", baudrate=9600)
# Configuration:
LED_PIN = 24
BUTTON_PIN = 23
# Initialize GPIO for LED and button.
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(LED_PIN, GPIO.OUT)
GPIO.output(LED_PIN, GPIO.LOW)
GPIO.setup(BUTTON_PIN, GPIO.IN, pull_up_down=GPIO.PUD_UP)

# Setup callback functions that are called when MQTT events happen like
# connecting to the server or receiving data from a subscribed feed.


def on_connect(client, userdata, flags, rc):
   print("Connected with result code " + str(rc))
   # Subscribing in on_connect() means that if we lose the connection and
   # reconnect then subscriptions will be renewed.
   # client.subscribe("/leds/pi")
# The callback for when a PUBLISH message is received from the server.


def on_message(client, userdata, msg):
   print(msg.topic+" "+str(msg.payload))


   # Check if this is a message for the Pi LED.
   # if msg.topic == '/leds/pi':
       # Look at the message data and perform the appropriate action.
   #    if msg.payload == b'ON':
   #        GPIO.output(LED_PIN, GPIO.HIGH)
   #    elif msg.payload == b'OFF':
   #        GPIO.output(LED_PIN, GPIO.LOW)
   #    elif msg.payload == b'TOGGLE':
   #        GPIO.output(LED_PIN, not GPIO.input(LED_PIN))
# Create MQTT client and connect to localhost, i.e. the Raspberry Pi running
# this script and the MQTT server.
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.connect('localhost', 1883, 60)
# Connect to the MQTT server and process messages in a background thread.
client.loop_start()
# Main loop to listen for button presses.
print('Script is running, press Ctrl-C to quit...')
time.sleep(1)


def readArduinoSerial(micro):
        inicio = False
        fin = False
        #micro.flush()
        # print("Entro Serial")
        mensaje = ""
        caracter = ""
        
        while not inicio:
            caracter = micro.read(1)
            #print(caracter)
            #time.sleep(0.2)
           
            if(caracter == "{".encode()):
                inicio = True
        mensaje = caracter
        while not fin:
             caracter = micro.read(1)
             #print(caracter)
             if(caracter == "}".encode()):
                # print("Entro2")
                fin = True
                mensaje += caracter
             else:
                # print("Entro")
                #print(mensaje)
                #print("\n")
                mensaje += caracter
                
                
        # print(mensaje)
        return mensaje

    
    
def readSensors():
        
        
        txt = ""
        txt2 = ""
        caracter = ""
        inicio = False
        fin = False
        #
        
        
        arduino.write("S".encode())
        time.sleep(0.05)
        #print("enviando mensaje")
        #time.sleep(20)
        #print("Paso el tiempo")
        txt = readArduinoSerial(arduino)
      
        
        #print(txt)
        # txt_json=json.dumps(txt)
        # stringMFJ=json.loads(txt_json)
        stringMFJ = txt[1:len(txt)-1]
        #print (stringMFJ)
        arr = str(stringMFJ).split(",")

        if arr:
            isFirst = True;
            for x in arr:
                arr2 = x.split(":")
                # print (len(arr2))
                if len(arr2) > 1:
                        if(isFirst):
                            diccionarioSensores[arr2[0]
                                [3:len(arr2[0])-1]] = arr2[1]
                            isFirst = False
                        elif(arr2[0][1:len(arr2[0])-1] == "puerta"):
                           diccionarioSensores[arr2[0][1:len(
                               arr2[0])-1]] = arr2[1][0:len(arr2[1])-1]
                        else:
                        # print arr2[0]
                        # print arr2[1]
                            diccionarioSensores[arr2[0]
                                [1:len(arr2[0])-1]] = arr2[1]
        # txt2_json=json.dumps(txt2)
        # string2MFJ=json.loads(txt2_json)
        inicio = False
        fin = False
        #time.sleep(1)
        
        #time.sleep(0.0005)
        
        arduino2.write("D".encode())
        time.sleep(0.05)
        txt2 = readArduinoSerial(arduino2)
        
        string2MFJ = txt2[1:len(txt2)-1]
        arr = str(string2MFJ).split(",")
        #print (arr)
       
        if arr:
            isFirst = True

            for x in arr:

                arr2 = x.split(":")
                if len(arr2) > 1:
                        if(isFirst):
                            diccionarioSensores2[arr2[0][3:len(arr2[0])-1]] = arr2[1]
                            isFirst = False
                        elif(arr2[0][1:len(arr2[0])-1] == "agua"):
                           diccionarioSensores2[arr2[0][1:len(arr2[0])-1]] = arr2[1][0:len(arr2[1])-1]
                        else:
                                diccionarioSensores2[arr2[0][1:len(arr2[0])-1]] = arr2[1]

        return len(arr)

def is_empty(data_structure):
    if data_structure:
        print("No está vacía")
        return False
    else:
        print("Está vacía")
        return True



class Command(BaseCommand):

    def handle(self, *args, **options):
       
        while(True):
                ret = readSensors()
                print("Sensores Aire:")
                print(diccionarioSensores)
                print("Sensores Agua:")
                print(diccionarioSensores2)
                sensorTI= Sensor.objects.get(pk=1)
                sensorTI.valor= diccionarioSensores["temperatura"]
                sensorTI.save()
                sensorAL= Sensor.objects.get(pk=8)
                sensorAL.valor= diccionarioSensores["distancia"]
                sensorAL.save()
                sensorTE= Sensor.objects.get(pk=5)
                sensorTE.valor= diccionarioSensores2["temperatura_ext"]
                sensorTE.save()
                sensorHI= Sensor.objects.get(pk=3)
                sensorHI.valor= diccionarioSensores["humedad"]
                sensorHI.save()
                sensorHE= Sensor.objects.get(pk=2)
                sensorHE.valor= diccionarioSensores2["humedad_ext"]
                sensorHE.save()
                sensorAM= Sensor.objects.get(pk=27)
                sensorAM.valor= diccionarioSensores["amoniaco"]
                sensorAM.save()
                sensorCO= Sensor.objects.get(pk=18)
                sensorCO.valor= diccionarioSensores["co"]
                sensorCO.save()
                sensorNO2= Sensor.objects.get(pk=7)
                sensorNO2.valor= diccionarioSensores["no2"]
                sensorNO2.save()
                sensorPR= Sensor.objects.get(pk=25)
                sensorPR.valor= diccionarioSensores["propano"]
                sensorPR.save()
                sensorBU= Sensor.objects.get(pk=6)
                sensorBU.valor= diccionarioSensores["butano"]
                sensorBU.save()
                sensorME= Sensor.objects.get(pk=17)
                sensorME.valor= diccionarioSensores["metano"]
                sensorME.save()
                sensorH= Sensor.objects.get(pk=15)
                sensorH.valor= diccionarioSensores["hidrogeno"]
                sensorH.save()
                sensorET= Sensor.objects.get(pk=10)
                sensorET.valor= diccionarioSensores["etanol"]
                sensorET.save()
                sensorL= Sensor.objects.get(pk=16)
                sensorL.valor= diccionarioSensores["luminosidad"]
                sensorL.save()
                sensorPU= Sensor.objects.get(pk=26)
                sensorPU.valor= diccionarioSensores["puerta"]
                sensorPU.save()
                sensorNI= Sensor.objects.get(pk=19)
                sensorNI.valor= diccionarioSensores2["nitrogeno"]
                sensorNI.save()
                sensorFO= Sensor.objects.get(pk=11)
                sensorFO.valor= diccionarioSensores2["fosforo"]
                sensorFO.save()
                sensorK= Sensor.objects.get(pk=24)
                sensorK.valor= diccionarioSensores2["potasio"]
                sensorK.save()
                sensorPO= Sensor.objects.get(pk=4)
                sensorPO.valor= diccionarioSensores2["potencia"]
                sensorPO.save()
                sensorPH= Sensor.objects.get(pk=22)
                sensorPH.valor= diccionarioSensores2["ph"]
                sensorPH.save()
                sensorAG= Sensor.objects.get(pk=21)
                sensorAG.valor= diccionarioSensores2["agua"]
                sensorAG.save()

                estadoAmbiente = EA(temperaturaInterior=diccionarioSensores["temperatura"],
                                        temperaturaExterior = diccionarioSensores2["temperatura_ext"],
                                        humedadInterior = diccionarioSensores["humedad"],
                                        humedadExterior = diccionarioSensores2["humedad_ext"],
                                        amoniaco = diccionarioSensores["amoniaco"],
                                        co = diccionarioSensores["co"],
                                        no2 = diccionarioSensores["no2"],
                                        propano = diccionarioSensores["propano"],
                                        butano = diccionarioSensores["butano"],
                                        metano = diccionarioSensores["metano"],
                                        hidrogeno = diccionarioSensores["hidrogeno"],
                                        etanol = diccionarioSensores["etanol"],
                                        luminosidadInterior = diccionarioSensores["luminosidad"],
                                        distancia = diccionarioSensores["distancia"],
                                        puerta = diccionarioSensores["puerta"],
                                        nitrogeno = diccionarioSensores2["nitrogeno"],
                                        fosforo = diccionarioSensores2["fosforo"],
                                        potasio = diccionarioSensores2["potasio"],
                                        potencia = diccionarioSensores2["potencia"],
                                        ph = diccionarioSensores2["ph"],
                                        nivelAgua = diccionarioSensores2["agua"])
                estadoAmbiente.save()
                print("Creo el ambiente")
                time.sleep(0.5)



        # for poll_id in options['poll_ids']:
        #    try:
        #       poll = Poll.objects.get(pk=poll_id)
        #   except Poll.DoesNotExist:
        #      raise CommandError('Poll "%s" does not exist' % poll_id)

        # poll.opened = False
        # poll.save()

        # self.stdout.write(self.style.SUCCESS(
           # 'Successfully closed poll "%s"' % poll_id))

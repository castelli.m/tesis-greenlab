import serial
import fcntl
import time
import math
import logging
from greenLab.models import Actuador
from django.core.management.base import BaseCommand, CommandError
#from sistema.tasks import cambiar_valor

import sys
import glob

import paho.mqtt.client as mqtt


class Command(BaseCommand):

    # called while client tries to establish connection with the server
    def on_connect(self, mqttc, obj, flags, rc):
        if rc == 0:
            print("Subscriber Connection status code: " +
                  str(rc)+" | Connection status: successful")
            mqttc.subscribe("outTopic", qos=1)

        elif rc == 1:
            print("Subscriber Connection status code: "+str(rc) +
                  " | Connection status: Connection refused")

         # called when a topic is successfully subscribed to
    def on_subscribe(self, mqttc, obj, mid, granted_qos):
        print("Subscribed: "+str(mid)+" "+str(granted_qos)+"data"+str(obj))

    # called when a message is received by a topic
    def on_message(self, mqttc, obj, msg):
        print("Received message from topic: "+msg.topic+" | QoS: " +
              str(msg.qos)+" | Data Received: "+str(msg.payload))
        mensaje = str(msg.payload).split(";")
        print(str(mensaje[0][2:]))
        act_rele_0=Actuador.objects.filter(controlador__mac=str(mensaje[0][2:]).lower()).filter(id_rele='0')[0]
        act_rele_0.valor= int(mensaje[1])
        act_rele_0.save()
        act_rele_1=Actuador.objects.filter(controlador__mac=str(mensaje[0][2:]).lower()).filter(id_rele='1')[0]
        act_rele_1.valor= int(mensaje[2][0])
        act_rele_1.save()
        
    def handle(self, *args, **options):
        mqttc = mqtt.Client(client_id="mqtt-test", clean_session=False)
        mqttc.on_connect = self.on_connect
        mqttc.on_subscribe = self.on_subscribe
        mqttc.on_message = self.on_message
        mqttc.connect("127.0.0.1", port=1883)
        mqttc.loop_start()
        while True:
            var = 1

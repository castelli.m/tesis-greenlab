from django.shortcuts import render
from .models import Sensor,TipoSensor, Actuador
from django.http import HttpResponse
# Create your views here.

def index(request):
    
    tipos_sensores_list =TipoSensor.objects.all()
    sensores_list=Sensor.objects.all()
 

    return render(request,'index.html',{'sensores_list':sensores_list,'tipos_sensores_list':tipos_sensores_list})
    

def actuadores(request):
    
    tipos_actuadores_list =TipoSensor.objects.all()
    actuadores_list=Actuador.objects.all().order_by('id')
 

    return render(request,'actuadores.html',{'actuadores_list':actuadores_list,'tipos_actuadores_list':tipos_actuadores_list})



def on(request,idActuador):
    act=Actuador.objects.get(id=idActuador)
    if(act.id==15):
        act.valor=0
    elif act.id==16:
        act.valor=2
    else:
        act.valor=1
    act.save()
    return HttpResponse(request,"ok")
    
    
    

def off(request,idActuador):
    
    act=Actuador.objects.get(id=idActuador)
    if(act.id==15):
        act.valor=1
    elif act.id==16:
        act.valor=1
    else:
        act.valor=0
    act.save()
    return HttpResponse(request,"ok")

    

from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('actuadores/', views.actuadores, name="actuadores"),
    path('actuadores/on/<int:idActuador>/', views.on, name="on"),
    path('actuadores/off/<int:idActuador>/', views.off, name="off"),
 
]
